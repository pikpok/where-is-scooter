export const toSentenceCase = ((str: string) => {
    const uppercaseLetters = /(^\w{1}|\.\s+\w{1})/gi;

    return str.replace(uppercaseLetters, toReplace => toReplace.toUpperCase());
});
