import axios, { AxiosRequestConfig } from 'axios';
import { BLINKEE_API_URL } from './constants';

const apiCaller = <T>(options: AxiosRequestConfig) => {
    return axios.request<T>({
        baseURL: BLINKEE_API_URL,
        ...options,
    });
};

export const apiGet = <T>(url: string, options?: AxiosRequestConfig) =>
    apiCaller<T>({
        url,
        method: 'GET',
        ...options,
    });
