import { toSentenceCase } from './helpers';

describe('helpers', () => {
    it('should correctly transform string to `Sentence case`', () => {
        expect(toSentenceCase('single')).toBe('Single');
        expect(toSentenceCase('simple sentence?')).toBe('Simple sentence?');
        expect(toSentenceCase('test. loremIpsum')).toBe('Test. LoremIpsum');
        expect(toSentenceCase('Already correct')).toBe('Already correct');
        expect(toSentenceCase('...nevermind')).toBe('...nevermind');
        expect(toSentenceCase('case a.b.c. d')).toBe('Case a.b.c. D');
        expect(toSentenceCase('e-mail')).toBe('E-mail');
    });
});
