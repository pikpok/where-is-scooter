export type LatLngArray = [number, number];

export enum FETCHING_STATUS {
    'PENDING',
    'SUCCESS',
    'FAILURE',
}
