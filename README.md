# Where is scooter?

Simple Expo app showing [blinkee.city](https://blinkee.city) scooter and roller
locations in different regions.

## Installation

You can run it with Expo app by searching for `@pikpok/where-is-scooter` or
opening [this link](https://expo.io/@pikpok/where-is-scooter).

## Development

You need to have Node.js and Yarn installed. To install dependencies, run:

```sh
yarn
```

Then to run project execute:

```sh
yarn start
```

This should start development server and instructions how to run app on your
phone/emulator should show up in terminal.
