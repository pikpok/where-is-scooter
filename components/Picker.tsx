import React from 'react';
import { Picker as NativePicker, PickerProps as NativePickerProps, StatusBar } from 'react-native';
import styled from 'styled-components/native';

export type PickerItemValue = string | number | null;

export interface PickerItem {
    label: string;
    value: PickerItemValue;
}

export interface PickerProps {
    items: PickerItem[];
    value: PickerItemValue;
    onChange: (value: PickerItemValue) => void;
    pickerProps?: Partial<NativePickerProps>;
}

const statusBarHeight = StatusBar.currentHeight || 0;

const StyledPicker = styled(NativePicker)`
    margin-top: ${statusBarHeight ? statusBarHeight + 20 : 0}px;
    height: 50px;
    width: 100%;
    background: #fff;
`;

export const Picker = ({ items, value, onChange, pickerProps = {} }: PickerProps) => (
    <StyledPicker mode="dropdown" selectedValue={value} onValueChange={onChange} {...pickerProps} >
        {items.map(item => (
            <NativePicker.Item key={item.value} value={item.value} label={item.label} />
        ))}
    </StyledPicker>
);
