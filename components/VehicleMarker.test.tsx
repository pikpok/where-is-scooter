import React from 'react';
import { render } from 'react-native-testing-library';
import { Vehicle, VEHICLE_TYPE } from '../store/vehicles';
import { VehicleMarker } from './VehicleMarker';

const vehicle: Vehicle = {
    position: {
        lat: 0,
        lng: 0,
    },
    type: VEHICLE_TYPE.ROLLER,
};

describe('VehicleMarker component', () => {
    it('should render correctly', () => {
        const component = render(<VehicleMarker vehicle={vehicle} />);

        expect(component.toJSON()).toMatchSnapshot();
    });
});
