import React from 'react';
import { render } from 'react-native-testing-library';
import { ScooterAnimation } from './ScooterAnimation';

describe('ScooterAnimation component', () => {
    it('should render with applied styles', () => {
        const component = render(<ScooterAnimation />);

        expect(component.toJSON()).toMatchSnapshot();
    });
});
