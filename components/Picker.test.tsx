import React from 'react';
import { render } from 'react-native-testing-library';
import { Picker } from './Picker';

describe('Picker component', () => {
    it('should render with applied styles', () => {
        const testItems = [];
        const onChange = jest.fn();
        const component = render(<Picker items={testItems} onChange={onChange} value={null} />);

        expect(component.toJSON()).toMatchSnapshot();
    });
});
