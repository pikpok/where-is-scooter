import React from 'react';
import { Marker } from 'react-native-maps';
import { Vehicle } from '../store/vehicles';
import { toSentenceCase } from '../utils/helpers';

interface VehicleMarkerProps {
    vehicle: Vehicle;
}

export const VehicleMarker = ({ vehicle }: VehicleMarkerProps) => (
    <Marker
        coordinate={{
            latitude: vehicle.position.lat,
            longitude: vehicle.position.lng,
        }}
        title={toSentenceCase(vehicle.type)}
    />
);
