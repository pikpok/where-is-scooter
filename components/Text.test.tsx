import React from 'react';
import { render } from 'react-native-testing-library';
import { Text } from './Text';

describe('Text component', () => {
    it('should render with applied styles', () => {
        const component = render(<Text />);

        expect(component.toJSON()).toMatchSnapshot();
    });
});
