import LottieView from 'lottie-react-native';
import React from 'react';
import styled from 'styled-components/native';
import scooterAnimation from '../assets/scooter.json';

const AnimationContainer = styled.View`
    background: #868b8e;
    border-radius: ${Number.MAX_SAFE_INTEGER};
    padding: 20px;
`;

const StyledLottieView = styled(LottieView)`
    width: 40%;
`;

export const ScooterAnimation = () => (
    <AnimationContainer>
        <StyledLottieView source={scooterAnimation} autoPlay loop />
    </AnimationContainer>
);
