import React from 'react';
import { render } from 'react-native-testing-library';
import { Provider } from 'react-redux';
import { store } from '../store';
import { changeRegion, Region } from '../store/regions';
import { Map } from './Map';

jest.mock('react-native-maps', () => 'MapView');

describe('Map', () => {
    const setup = () => {
        const component = render((
            <Provider store={store}>
                <Map />
            </Provider>
        ));

        return { component };
    };

    it('should render correctly', () => {
        const { component } = setup();

        expect(component.toJSON()).toMatchSnapshot();
    });

    it('should update position when region is selected', () => {
        const { component } = setup();
        const region: Region = {
            center: [20, 30],
            id: 7,
            name: 'Test Region',
        };

        store.dispatch(changeRegion(region));
        component.update(<Provider store={store}><Map /></Provider>);

        const mapViewProps = component.toJSON().props;

        expect(mapViewProps.region.latitude).toBe(region.center[1]);
        expect(mapViewProps.region.longitude).toBe(region.center[0]);
    });
});
