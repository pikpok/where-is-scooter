import React, { useEffect, useState } from 'react';
import { Animated, TouchableOpacity } from 'react-native';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';
import { Picker, PickerItem, PickerItemValue } from '../components/Picker';
import { Text } from '../components/Text';
import { State } from '../store';
import { changeRegion, fetchRegions, Region } from '../store/regions';
import { FETCHING_STATUS } from '../utils/types';

const Container = styled(Animated.View)`
    position: absolute;
    padding: 0 20px;
    width: 100%;
    height: 100%;
    align-items: center;
`;

const PLACEHOLDER_ITEM: PickerItem = { value: null, label: 'Select region...' };

export const RegionPicker = () => {
    const dispatch = useDispatch();
    const currentRegion = useSelector<State, Region | null>(
        state => state.regions.current,
        shallowEqual,
    );
    const regions = useSelector<State, Region[]>(state => state.regions.available, shallowEqual);
    const fetchStatus = useSelector<State, FETCHING_STATUS>(state => state.regions.status);
    const [positionAnimation] = useState(new Animated.Value(100));
    const [backgroundAnimation] = useState(new Animated.Value(100));

    const getRegionById = (value: PickerItemValue) => regions.find(({ id }) => id === value);

    useEffect(() => {
        Animated.timing(positionAnimation, {
            toValue: currentRegion ? 0 : 100,
            duration: 500,
        }).start();
        Animated.timing(backgroundAnimation, {
            toValue: currentRegion ? 0 : 100,
            duration: 300,
        }).start();
    }, [!!currentRegion]);

    useEffect(() => {
        dispatch(fetchRegions());
    }, [dispatch]);

    const pickerItems: PickerItem[] = [
        PLACEHOLDER_ITEM,
        ...regions.map(item => ({ value: item.id, label: item.name })),
    ];

    return (
        <Container
            style={{
                paddingTop: positionAnimation.interpolate({
                    inputRange: [0, 100],
                    outputRange: ['0%', '100%'],
                }),
                backgroundColor: backgroundAnimation.interpolate({
                    inputRange: [0, 100],
                    outputRange: ['rgba(231, 210, 204, 0)', 'rgba(231, 210, 204, 1)'],
                }),
            }}
            pointerEvents="box-none"
        >
            <Picker
                value={currentRegion ? currentRegion.id : null}
                onChange={value => dispatch(changeRegion(getRegionById(value)))}
                items={pickerItems}
            />

            {fetchStatus === FETCHING_STATUS.FAILURE && (
                <TouchableOpacity onPress={() => dispatch(fetchRegions())}>
                    <Text>Failed to fetch regions, tap to retry</Text>
                </TouchableOpacity>
            )}
        </Container>
    );
};
