import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import MapView, { Region as MapRegion } from 'react-native-maps';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components/native';
import { VehicleMarker } from '../components/VehicleMarker';
import { State } from '../store';
import { Region } from '../store/regions';
import { fetchVehicles, Vehicle } from '../store/vehicles';

const StyledMapView = styled(MapView)`
    flex: 1;
    width: 100%;
    height: 100%;
`;

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export const Map = () => {
    const dispatch = useDispatch();
    const region = useSelector<State, Region | null>(state => state.regions.current, shallowEqual);
    const vehicles = useSelector<State, Vehicle[]>(state => state.vehicles.vehicles, shallowEqual);
    const [mapRegion, setMapRegion] = useState<MapRegion>(null);

    useEffect(() => {
        let latitude = 0;
        let longitude = 0;

        if (region && region.center) {
            latitude = region.center[1];
            longitude = region.center[0];

            dispatch(fetchVehicles(region.id));

            setMapRegion({
                ...mapRegion,
                latitude,
                longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            });
        }

    }, [region]);

    return (
        <StyledMapView
            region={mapRegion}
            onRegionChangeComplete={setMapRegion}
            showsUserLocation
            showsMyLocationButton={false}
        >
            {vehicles.map(vehicle => (
                <VehicleMarker
                    key={`${vehicle.position.lat}-${vehicle.position.lng}`}
                    vehicle={vehicle}
                />
            ))}
        </StyledMapView>
    );
};
