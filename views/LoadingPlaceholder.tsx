import React, { useEffect, useState } from 'react';
import { Animated } from 'react-native';
import { useSelector } from 'react-redux';
import styled from 'styled-components/native';
import { ScooterAnimation } from '../components/ScooterAnimation';
import { Text } from '../components/Text';
import { State } from '../store';
import { FETCHING_STATUS } from '../utils/types';

const Container = styled(Animated.View)`
    position: absolute;
    width: 100%;
    height: 100%;
    background: #eeede7;
    flex: 1;
    align-items: center;
    justify-content: center;
`;

const ERROR_TEXT = 'Something went wrong, please try again';
const LOADING_TEXT = 'Loading vehicles...';

export const LoadingPlaceholder = () => {
    const [positionAnimation] = useState(new Animated.Value(0));
    const fetchingStatus = useSelector<State, FETCHING_STATUS>(state => state.vehicles.status);

    const hasError = fetchingStatus === FETCHING_STATUS.FAILURE;
    const shouldShow = fetchingStatus !== FETCHING_STATUS.SUCCESS;

    useEffect(() => {
        if (shouldShow) {
            positionAnimation.setValue(0);
        } else {
            Animated.timing(positionAnimation, {
                toValue: 100,
                duration: 400,
            }).start();
        }
    }, [shouldShow]);

    return (
        <Container
            style={{
                top: positionAnimation.interpolate({
                    inputRange: [0, 100],
                    outputRange: ['0%', '-100%'],
                }),
            }}
        >
            <ScooterAnimation />
            <Text>{hasError ? ERROR_TEXT : LOADING_TEXT}</Text>
        </Container>
    );
};
