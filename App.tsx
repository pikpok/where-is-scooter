import React from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import styled from 'styled-components/native';
import { store } from './store';
import { LoadingPlaceholder } from './views/LoadingPlaceholder';
import { Map } from './views/Map';
import { RegionPicker } from './views/RegionPicker';

const AppContainer = styled.View`
    flex: 1;
    background-color: #000;
    align-items: center;
    justify-content: center;
`;

export default () => {
    return (
        <Provider store={store}>
            <AppContainer>
                <StatusBar translucent barStyle="dark-content" />

                <Map />
                <LoadingPlaceholder />
                <RegionPicker />
            </AppContainer>
        </Provider>
    );
};
