import { Action, configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { ThunkAction } from 'redux-thunk';
import regions from './regions';
import vehicles from './vehicles';

const reducer = combineReducers({
    regions,
    vehicles,
});

export const store = configureStore({
    reducer,
});

export type State = ReturnType<typeof reducer>;
export type AppThunk = ThunkAction<void, State, null, Action<string>>;
