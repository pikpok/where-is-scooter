import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '.';
import { apiGet } from '../utils/api';
import { FETCHING_STATUS } from '../utils/types';

export enum VEHICLE_TYPE {
    ROLLER = 'roller',
    SCOOTER = 'scooter',
}

export interface VehiclePosition {
    lat: number;
    lng: number;
}

export interface Vehicle {
    type: VEHICLE_TYPE;
    position: VehiclePosition;
}

export interface VehiclesState {
    vehicles: Vehicle[];
    status: FETCHING_STATUS;
}

const initialState: VehiclesState = {
    vehicles: [],
    status: FETCHING_STATUS.SUCCESS,
};

const vehicles = createSlice({
    name: 'vehicles',
    initialState,
    reducers: {
        setStatus(state, action: PayloadAction<FETCHING_STATUS>) {
            state.status = action.payload;
        },
        setVehicles(state, action: PayloadAction<Vehicle[]>) {
            state.vehicles = action.payload;
        },
    },
});

export const { setVehicles, setStatus } = vehicles.actions;

export default vehicles.reducer;

export const fetchVehicles = (regionId: number): AppThunk => async (dispatch) => {
    try {
        dispatch(setStatus(FETCHING_STATUS.PENDING));

        const { data } = await apiGet<Vehicle[]>(`vehicles/${regionId}`);

        dispatch(setVehicles(data));
        dispatch(setStatus(FETCHING_STATUS.SUCCESS));
    } catch (error) {
        dispatch(setStatus(FETCHING_STATUS.FAILURE));
    }
};
