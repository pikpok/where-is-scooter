import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AppThunk } from '.';
import { apiGet } from '../utils/api';
import { FETCHING_STATUS, LatLngArray } from '../utils/types';

export interface Region {
    id: number;
    name: string;
    center: LatLngArray;
}

export interface RegionsState {
    available: Region[];
    status: FETCHING_STATUS;
    current?: Region;
}

const initialState: RegionsState = {
    available: [],
    status: FETCHING_STATUS.SUCCESS,
    current: null,
};

const regionsSlice = createSlice({
    name: 'regions',
    initialState,
    reducers: {
        setStatus(state, action: PayloadAction<FETCHING_STATUS>) {
            state.status = action.payload;
        },
        changeRegion(state, action: PayloadAction<Region | null>) {
            state.current = action.payload;
        },
        setRegions(state, action: PayloadAction<Region[]>) {
            state.available = action.payload;
        },
    },
});

export const { changeRegion, setRegions, setStatus } = regionsSlice.actions;

export default regionsSlice.reducer;

export const fetchRegions = (): AppThunk => async (dispatch) => {
    try {
        dispatch(setStatus(FETCHING_STATUS.PENDING));

        const { data } = await apiGet<Region[]>('regions');
        const regions = data.map(({ id, name, center }) => ({ id, name, center }));

        dispatch(setRegions(regions));
        dispatch(setStatus(FETCHING_STATUS.SUCCESS));
    } catch (error) {
        dispatch(setStatus(FETCHING_STATUS.FAILURE));
    }
};
